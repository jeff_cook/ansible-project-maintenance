FROM registry.gitlab.com/jeff_cook/ansible-playbook:v0.1.0

COPY requirements.yml /requirements.yml
RUN ansible-galaxy install -r /requirements.yml

COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

ENV ANSIBLE_CALLBACK_WHITELIST=junit
ENV JUNIT_FAIL_ON_CHANGE=true
ENV JUNIT_FAIL_ON_IGNORE=true
ENV JUNIT_HIDE_TASK_ARGUMENTS=true
ENV JUNIT_INCLUDE_SETUP_TASKS_IN_REPORT=false
ENV JUNIT_OUTPUT_DIR=./reports
# ENV JUNIT_TASK_RELATIVE_PATH

COPY playbook.yml /project/
COPY roles/ /project/roles/
COPY inventory/ /project/inventory/
