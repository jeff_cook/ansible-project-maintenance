# 1.0.0 (2021-02-21)


### Features

* add CI job to run project-lint ([297ae9f](https://gitlab.com/jeff_cook/ansible-project-maintenance/commit/297ae9fa83d7fc9f65e14c09fc29632cbe89d50e))
* create junit report ([c22a886](https://gitlab.com/jeff_cook/ansible-project-maintenance/commit/c22a8866053a3d6576806cb3643104ef382c1622))
* ensure .gitignore meets org standards ([e908934](https://gitlab.com/jeff_cook/ansible-project-maintenance/commit/e908934bf7950a9a5b567f96272ae4e1a03ec45d))
* manage CODEOWNERS ([e116aa5](https://gitlab.com/jeff_cook/ansible-project-maintenance/commit/e116aa58d7bd7e6b2d1f4f0e24a262601dfe67a8))
